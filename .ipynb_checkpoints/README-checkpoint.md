[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Cthulhus_Queen%2Fbarometre_scienceouverte_universitedelorraine/master?filepath=barometre_universite_lorraine.ipynb)


Travail inspiré du Baromètre français de la Science Ouverte : https://ministeresuprecherche.github.io/bso/

1) Téléchargez l'ensemble du Baromètre lorrain sur le bureau en utilisant le bouton "Download". Dé-zippez l'archive.

2) Installez la suite Anaconda Navigator (https://www.anaconda.com/products/individual).

3) Lancez Anaconda et sélectionnez Jupyter Lab. Le dossier du Baromètre lorrain téléchargé sur le bureau apparaît.

4) Remplacez les données de l'Université de Lorraine par celles de votre établissement. 
La liste des requêtes utilisées est dans le fichier "requetes_bdd".
Les extractions dans les bases de données doivent se faire exactement comme c'est expliqué dans le notebook qui s'appelle "nettoyage_donnees".
Il faut effacer le contenu du dossier 'outputs' pour enlever les graphiques lorrains (mais conserver le dossier vide).

5) Dans le dossier qui est sur le bureau, enlever les fichiers lorrains dans le dossier Data/raw et les remplacer par les vôtres. 
Attention, il faut reproduire très exactement le nommage des fichiers. Dans le code lorrain par exemple, le fichier 2016 du Web of Science 
s'appelle "wos_lorraine_2016". Remplacez les occurrences de "lorraine"
par le nom de votre établissement.

6) Ouvrir le notebook "nettoyage_donnees". Remplacer toutes les occurrences de "lorraine" par le nom de l'établissement (attention : utiliser le même nom que pour les extractions de bases de données).

7) Exécutez le notebook "nettoyage_donnees" en lisant bien les instructions à chaque étape.

8) Ouvrir le notebook "barometre_universite_lorraine". Remplacer toutes les occurrences de "lorraine" par le nom de l'établissement (attention : utiliser le même nom que pour les extractions de bases de données). Vous pouvez aussi renommer le notebook.

7) Exécutez le notebook "barometre_universite_lorraine" en lisant toutes les instructions.

8) Exécutez le notebook "clustering" en lisant toutes les instructions.

9) Les graphiques ont été automatiquement générés dans le dossier Data/outputs.